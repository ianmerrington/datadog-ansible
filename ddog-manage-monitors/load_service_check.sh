#!/bin/sh
# Make sure you replace the API and/or APP key below
# with the ones for your account

api_key=09efb1b36e9535917a8d6c3691eaa386
app_key=23e17b0e4c05f6b7da16df10fbb9210d83288ba6

for f in ./service_checks/*
do
  curl -X POST -H "Content-type: application/json" --data-binary "@$f" "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
done
