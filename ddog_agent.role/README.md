Role Name
=========

Role to install ddog agent in server

Requirements
------------

None

Role Variables
--------------

install_couchbase: false        #Install Couchbase integration
install_rabbitmq: false         #Install RabbitMQ integration

couchbase_user: Administrator   #Couchbase users and password
couchbase_password: seenit

agent_intergration_home: /etc/dd-agent/conf.d/      #Installation dir for intergrations
ddog_api_key: 09efb1b36e9535917a8d6c3691eaa386      #API Key for DDOG

Dependencies
------------

None

Example Playbook
----------------

- name: apply ddog to all nodes
  hosts: all
  become: true

  roles:
    - datadog.role


License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
