Data Dog ansible Scriptse
=========

ddog-install-agent
-----------------
Install the agent on debian hosts

ddog-manage-monitors
-------------------
Add or modify datadog monitors

ddog_agent.role
--------------
Role used by ddog-isntall-agent

ddog_monitor.role
----------------
Role used by ddog-manage-monitors

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
